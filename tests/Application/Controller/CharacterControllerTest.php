<?php

namespace App\Tests\Application\Controller;

use ApiPlatform\Symfony\Bundle\Test\ApiTestCase;
use Symfony\Component\HttpFoundation\Response;

class CharacterControllerTest extends ApiTestCase
{
    /** @test */
    public function mustFailsWithEmptyBody(): void
    {
        static::createClient()->request('POST', '/api/character');

        $this->assertResponseStatusCodeSame(Response::HTTP_BAD_REQUEST);
    }

    /** @test */
    public function mustReturnCreatedWithValidBody(): void
    {
        static::createClient()->request(
            method: 'POST',
            url: '/api/character',
            options: ['json' => ['characterName' => '::characterName::', 'actorName' => '::actorName::']]
        );

        $this->assertResponseStatusCodeSame(Response::HTTP_CREATED);
    }

    /** @test */
    public function mustReturnNotFoundWhenIdNotExists(): void
    {
        static::createClient()->request(
            method: 'GET',
            url: '/api/character/1'
        );

        $this->assertResponseStatusCodeSame(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function mustReturnTheCharacterWhenIdExists(): void
    {
        $expectedResponse = '{"characterName":"::characterName::","actorName":"::actorName::","id":1,"siblings":[]}';
        static::createClient()->request(
            method: 'POST',
            url: '/api/character',
            options: ['json' => ['characterName' => '::characterName::', 'actorName' => '::actorName::']]
        );

        $this->assertResponseStatusCodeSame(Response::HTTP_CREATED);

        $client = static::createClient()->request(
            method: 'GET',
            url: '/api/character/1'
        );

        $response = $client->getContent();

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
        $this->assertEquals($expectedResponse, $response);
    }
}
