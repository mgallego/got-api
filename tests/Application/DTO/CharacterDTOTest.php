<?php

namespace App\Tests\Application\DTO;

use App\Application\DTO\CharacterDTO;
use App\Domain\Model\Character;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;

class CharacterDTOTest extends TestCase
{
    /** @test  */
    public function mustReturnAModelObjectWithTheRequestData()
    {
        $request = new Request(
            [],
            ['characterName' => '::characterName::', 'actorName' => '::actorName::', 'siblings' => ['::sibling1::']]
        );
        $expectedObject = new Character(
            characterName: '::characterName::',
            actorName: '::actorName::',
            siblings: ['::sibling1::']
        );

        $dto = new CharacterDTO($request);
        $character = $dto->toCharacterModel();

        $this->assertEquals($expectedObject, $character);
    }
}
