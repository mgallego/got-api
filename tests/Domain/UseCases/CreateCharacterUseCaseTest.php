<?php

namespace App\Tests\Domain\UseCases;

use App\Domain\Model\Character;
use App\Domain\Repository\CharacterRepositoryInterface;
use App\Domain\UseCases\CreateCharacterUseCase;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;

class CreateCharacterUseCaseTest extends TestCase
{
    use ProphecyTrait;

    /** @test */
    public function repositorySaveIsCalled()
    {
        $characterRepository = $this->prophesize(CharacterRepositoryInterface::class);
        $repo = $characterRepository->reveal();

        $character = new Character(
            characterName: '::characterName::',
            actorName: '::actorName::',
            siblings: ['::sibling1::']
        );

        $result = new CreateCharacterUseCase($repo);

        $result->execute($character);

        $characterRepository->save($character)->shouldBeCalled();
    }
}
