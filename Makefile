current-dir := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

.PHONY: build
build: deps start load-databases

.PHONY: deps
deps: composer-install

# 🐘 Composer
composer-env-file:
	@if [ ! -f .env.local ]; then echo '' > .env.local; fi

.PHONY: composer-install
composer-install: CMD=install

.PHONY: composer-update
composer-update: CMD=update

.PHONY: composer-require
composer-require: CMD=require
composer-require: INTERACTIVE=-ti --interactive

.PHONY: composer-require-module
composer-require-module: CMD=require $(module)
composer-require-module: INTERACTIVE=-ti --interactive

.PHONY: composer
composer composer-install composer-update composer-require composer-require-module: composer-env-file
	@docker run --rm $(INTERACTIVE) --volume $(current-dir):/app --user $(id -u):$(id -g) \
		composer:2.3.7 $(CMD) \
			--ignore-platform-reqs \
			--no-ansi

.PHONY: reload
reload: composer-env-file
	docker compose exec php-fpm kill -USR2 1
	docker compose exec nginx nginx -s reload

.PHONY: test
test: composer-env-file
	docker exec whalar-test-rest-api ./vendor/bin/phpunit

.PHONY: test-with-coverage
test-with-coverage: composer-env-file
	docker exec -e XDEBUG_MODE=coverage whalar-test-rest-api ./vendor/bin/phpunit --coverage-html ./var/coverage


.PHONY: static-analysis
static-analysis: composer-env-file
	docker exec whalar-test-rest-api ./vendor/bin/phpstan analyse src
	docker exec whalar-test-rest-api ./vendor/bin/phpcs src --standard=PSR2
	docker exec whalar-test-rest-api ./vendor/bin/phpmd src/ text phpmd.xml

.PHONY: lint
lint:
	docker exec whalar-test-rest-api ./vendor/bin/php-cs-fixer fix --config .php-cs-fixer.dist.php --allow-risky=yes --dry-run

.PHONY: exec-consumer
exec-consumer:
	docker exec whalar-test-rest-api ./bin/console rabbitmq:consumer loadCharacter

.PHONY: build-es
build-es:
	docker exec whalar-test-rest-api php bin/console fos:elastica:populate

# 🐳 Docker Compose
.PHONY: start
start:
	docker compose up --build -d

.PHONY: stop
stop:
	docker compose stop

.PHONY: destroy
destroy:
	docker compose down

clean-cache:
	@rm -rf apps/*/*/var

.PONY: load-databases
load-databases:
	docker exec whalar-test-rest-api php bin/console --env=test doctrine:database:drop --force
	docker exec whalar-test-rest-api php bin/console --env=test doctrine:database:create
	docker exec whalar-test-rest-api php bin/console --env=test doctrine:schema:create
	docker exec whalar-test-rest-api php bin/console --env=dev doctrine:database:drop --force
	docker exec whalar-test-rest-api php bin/console --env=dev doctrine:database:create
	docker exec whalar-test-rest-api php bin/console --env=dev doctrine:schema:create

