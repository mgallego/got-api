# GOT API REST

In order to execute all the code yo have the option to use make tasks or directly through docker. The make tasks execute the same docker commands

## Installation

Everything is created and running with Make. To create the Docker image, and run the application you need to execute the next command in your terminal:

`make`

This command do the next things:

- Create the Docker Image
- Install composer dependencies
- Create dev and test databases
- Start the application with docker compose

To stop the application execute `make stop`

## Usage

With `make` command the application is created and running, but if you stop the execution with `make stop` you can start the application again with `make start`. In order to execute the next steps is needed that the application will be running.
## Exec commands into Docker Container

In case you need to execute some code into the container you can execute the next command. For the code challenge this step is not needed, only informative.

`docker run -it whalar_api_rest /bin/sh`


## Testing

### With Make

- **Only Test Execution** `make test`
- **With coverage** `make test-with-coverage`

### With Docker

- **Only Test Execution** `docker exec whalar-test-rest-api ./vendor/bin/phpunit`
- **With coverage** `docker exec -e XDEBUG_MODE=coverage whalar-test-rest-api ./vendor/bin/phpunit --coverage-html ./var/coverage`


### Coverage

Exists a current coverage report in text file located [here](doc/coverage.txt) but with the execution of `make test-with-coverage` you can create html reports which will be stored in `./var/coverage` folder.

## Analysis and Lint

The command to execute the analysis and linting tools is `make static-analysis` for analysis and `make lint` for lint. 

## OpenAPI Documentation

You can see the OpenAPI specification [here](doc/openapi.yml).


## ElasticSearch

In order to create and load the index you can execute the next commands

### With Make

`make build-es`

### With docker

`docker exec whalar-test-rest-api php bin/console fos:elastica:populate`

### Information about Elastic

Once this command is executed, the index will be created and loaded. If a new record is created in DB or an existing record is modified, the index will be automatically updated without the need to execute any other command.


## RabbitMQ

There is a RabbitMQ consumer that receives a character object as defined in OpenAPI and stores it in DB and indexes it in ElasticSearch.

To start this consumer we can do it in the following way:

### With Make
`make exec-consumer`

### With Docker 
`docker exec whalar-test-rest-api ./bin/console rabbitmq:consumer loadCharacter`

## Client

Due to lack of time I have not been able to create a client. My idea was to create an example in the infrastructure folder with Guzzle and configured in the Application part.