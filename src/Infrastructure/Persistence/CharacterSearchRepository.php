<?php

namespace App\Infrastructure\Persistence;

use App\Domain\Model\Character;
use App\Domain\Model\CharacterCollection;
use App\Domain\Repository\CharacterSearchRepositoryInterface;
use FOS\ElasticaBundle\Manager\RepositoryManagerInterface;

class CharacterSearchRepository implements CharacterSearchRepositoryInterface
{
    private RepositoryManagerInterface $repositoryManager;

    public function __construct(RepositoryManagerInterface $repositoryManager)
    {
        $this->repositoryManager = $repositoryManager;
    }

    public function search(string $term): CharacterCollection
    {
        $repository = $this->repositoryManager->getRepository(Character::class);

        $characters = $repository->find($term);

        return new CharacterCollection($characters);
    }

    public function index(Character $character): void
    {
        // TODO: Implement index() method. Not needed for the code challenge, automatically indexed by elastica
    }
}
