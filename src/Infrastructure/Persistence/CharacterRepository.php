<?php

namespace App\Infrastructure\Persistence;

use App\Domain\Exception\NotFoundException;
use App\Domain\Model\Character;
use App\Domain\Repository\CharacterRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class CharacterRepository extends ServiceEntityRepository implements CharacterRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Character::class);
    }

    public function save(Character $character): void
    {
        $entityManager = $this->getEntityManager();

        $entityManager->persist($character);
        $entityManager->flush();
    }

    public function update(Character $character): void
    {
        $this->get($character->getId());
        $entityManager = $this->getEntityManager();

        $entityManager->merge($character);
        $entityManager->flush();
    }

    /**
     * @throws NotFoundException
     */
    public function get(int $id): Character
    {
        return $this->find($id) ?? throw new NotFoundException();
    }

    public function delete(int $id): void
    {
        $character = $this->get($id);

        $entityManager = $this->getEntityManager();
        $entityManager->remove($character);
        $entityManager->flush();
    }
}
