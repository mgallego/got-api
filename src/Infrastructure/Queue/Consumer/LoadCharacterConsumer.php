<?php

namespace App\Infrastructure\Queue\Consumer;

use App\Domain\Model\Character;
use App\Domain\UseCases\CreateCharacterUseCase;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Component\Serializer\SerializerInterface;

class LoadCharacterConsumer implements ConsumerInterface
{
    public function __construct(private SerializerInterface $serializer, private CreateCharacterUseCase $useCase)
    {
    }

    public function execute(AMQPMessage $msg): bool
    {
        try {
            $character = $this->serializer->deserialize($msg->body, Character::class, 'json');
            $this->useCase->execute($character);

            return true;
        } catch (\Exception) {
            return false;
        }
    }
}
