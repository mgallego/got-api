<?php

namespace App\Application\Controller;

use App\Application\DTO\CharacterDTO;
use App\Domain\Exception\NotFoundException;
use App\Domain\UseCases\CreateCharacterUseCase;
use App\Domain\UseCases\DeleteCharacterUseCase;
use App\Domain\UseCases\GetCharacterUseCase;
use App\Domain\UseCases\SearchCharacterUseCase;
use App\Domain\UseCases\UpdateCharacterUseCase;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validation;

class CharacterController extends AbstractFOSRestController
{
    public function post(Request $request, CreateCharacterUseCase $useCase): Response
    {
        $validationErrors = $this->validateRequest($request);

        if ($validationErrors->count() > 0) {
            return $this->handleView($this->view(null, Response::HTTP_BAD_REQUEST));
        }

        $dto = new CharacterDTO($request);
        $useCase->execute($dto->toCharacterModel());

        return $this->handleView($this->view(null, Response::HTTP_CREATED));
    }

    public function get(Request $request, int $id, GetCharacterUseCase $useCase): Response
    {
        try {
            $character = $useCase->execute($id);

            $view = $this->view($character, Response::HTTP_OK);
        } catch (NotFoundException) {
            $view = $this->view(null, Response::HTTP_NOT_FOUND);
        }

        return $this->handleView($view);
    }

    public function put(Request $request, UpdateCharacterUseCase $useCase): Response
    {
        $validationErrors = $this->validateRequest($request);

        if ($validationErrors->count() > 0) {
            return $this->handleView($this->view(null, Response::HTTP_BAD_REQUEST));
        }

        try {
            $dto = new CharacterDTO($request);
            $useCase->execute($dto->toCharacterModel());

            $view = $this->view(null, Response::HTTP_OK);
        } catch (NotFoundException) {
            $view = $this->view(null, Response::HTTP_NOT_FOUND);
        }

        return $this->handleView($view);
    }

    public function delete(Request $request, int $id, DeleteCharacterUseCase $useCase): Response
    {
        try {
            $useCase->execute($id);

            $view = $this->view(null, Response::HTTP_OK);
        } catch (NotFoundException) {
            $view = $this->view(null, Response::HTTP_NOT_FOUND);
        }

        return $this->handleView($view);
    }

    public function search(Request $request, string $term, SearchCharacterUseCase $useCase): Response
    {
        $characterCollection = $useCase->execute($term);
        $view = $this->view($characterCollection, Response::HTTP_OK);

        return $this->handleView($view);
    }

    private function validateRequest(Request $request): ConstraintViolationListInterface
    {
        $constraint = new Assert\Collection(
            [
                'characterName' => [new Assert\NotBlank(), new Assert\Length(['min' => 1, 'max' => 255])],
                'actorName' => [new Assert\NotBlank(), new Assert\Length(['min' => 1, 'max' => 255])],
            ]
        );

        $input = array_filter(
            $request->request->all(),
            function ($paramName) {
                $fieldsToValidate = ['characterName', 'actorName'];

                return in_array($paramName, $fieldsToValidate);
            },
            ARRAY_FILTER_USE_KEY
        );

        return Validation::createValidator()->validate($input, $constraint);
    }
}
