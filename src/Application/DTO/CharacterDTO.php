<?php

namespace App\Application\DTO;

use App\Domain\Model\Character;
use Symfony\Component\HttpFoundation\Request;

final class CharacterDTO
{
    public function __construct(private readonly Request $request)
    {
    }

    public function toCharacterModel(): Character
    {
        $request = $this->request->request;

        return new Character(
            characterName: $request->get('characterName'),
            actorName: $request->get('actorName'),
            id: $request->get('id', null),
            characterLink: $request->get('characterLink', null),
            actorLink: $request->get('actorLink', null),
            houseName: $request->get('houseName', null),
            siblings: $request->all('siblings')
        );
    }
}
