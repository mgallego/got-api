<?php

namespace App\Domain\Repository;

use App\Domain\Model\Character;
use App\Domain\Model\CharacterCollection;

interface CharacterSearchRepositoryInterface
{
    public function search(string $term): CharacterCollection;

    public function index(Character $character): void;
}
