<?php

namespace App\Domain\Repository;

use App\Domain\Model\Character;

interface CharacterRepositoryInterface
{
    public function save(Character $character): void;

    public function update(Character $character): void;

    public function get(int $id): Character;

    public function delete(int $id): void;
}
