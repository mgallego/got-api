<?php

namespace App\Domain\UseCases;

use App\Domain\Model\CharacterCollection;
use App\Domain\Repository\CharacterSearchRepositoryInterface;

final class SearchCharacterUseCase
{
    public function __construct(private CharacterSearchRepositoryInterface $repository)
    {
    }

    public function execute(string $term): CharacterCollection
    {
        return $this->repository->search($term);
    }
}
