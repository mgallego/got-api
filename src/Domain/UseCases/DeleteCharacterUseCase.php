<?php

namespace App\Domain\UseCases;

use App\Domain\Repository\CharacterRepositoryInterface;

final class DeleteCharacterUseCase
{
    public function __construct(private CharacterRepositoryInterface $repository)
    {
    }

    public function execute(int $id): void
    {
        $this->repository->delete($id);
    }
}
