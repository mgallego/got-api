<?php

namespace App\Domain\UseCases;

use App\Domain\Model\Character;
use App\Domain\Repository\CharacterRepositoryInterface;

final class UpdateCharacterUseCase
{
    public function __construct(private CharacterRepositoryInterface $repository)
    {
    }

    public function execute(Character $character): void
    {
        $this->repository->update($character);
    }
}
