<?php

namespace App\Domain\UseCases;

use App\Domain\Model\Character;
use App\Domain\Repository\CharacterRepositoryInterface;

final class GetCharacterUseCase
{
    public function __construct(private CharacterRepositoryInterface $repository)
    {
    }

    public function execute(int $id): Character
    {
        return $this->repository->get($id);
    }
}
