<?php

namespace App\Domain\UseCases;

use App\Domain\Model\Character;
use App\Domain\Repository\CharacterRepositoryInterface;

final class CreateCharacterUseCase
{
    public function __construct(private CharacterRepositoryInterface $repository)
    {
    }

    public function execute(Character $character)
    {
        $this->repository->save($character);
    }
}
