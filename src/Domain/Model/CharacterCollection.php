<?php

namespace App\Domain\Model;

class CharacterCollection extends Collection
{
    protected function type(): string
    {
        return Character::class;
    }
}
