<?php

namespace App\Domain\Model;

abstract class Collection implements \Countable, \IteratorAggregate
{
    public function __construct(private readonly array $items)
    {
    }

    abstract protected function type(): string;

    public function getIterator(): \ArrayIterator
    {
        return new \ArrayIterator($this->items());
    }

    public function count(): int
    {
        return count($this->items());
    }

    protected function items(): array
    {
        return $this->items;
    }
}
