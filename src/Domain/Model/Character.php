<?php

namespace App\Domain\Model;

class Character
{
    public function __construct(
        private string $characterName,
        private string $actorName,
        private ?int $id = null,
        private ?string $characterLink = null,
        private ?string $actorLink = null,
        private ?string $houseName = null,
        private ?array $siblings = []
    ) {
    }

    public function getCharacterName(): string
    {
        return $this->characterName;
    }

    public function setCharacterName(string $characterName): void
    {
        $this->characterName = $characterName;
    }

    public function getActorName(): string
    {
        return $this->actorName;
    }

    public function setActorName(string $actorName): void
    {
        $this->actorName = $actorName;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCharacterLink(): ?string
    {
        return $this->characterLink;
    }

    public function setCharacterLink(?string $characterLink): void
    {
        $this->characterLink = $characterLink;
    }

    public function getActorLink(): ?string
    {
        return $this->actorLink;
    }

    public function setActorLink(?string $actorLink): void
    {
        $this->actorLink = $actorLink;
    }

    public function getHouseName(): ?string
    {
        return $this->houseName;
    }

    public function setHouseName(?string $houseName): void
    {
        $this->houseName = $houseName;
    }

    public function getSiblings(): ?array
    {
        return $this->siblings;
    }

    public function setSiblings(?array $siblings): void
    {
        $this->siblings = $siblings;
    }
}
